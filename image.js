import React, {PureComponent} from 'react';
import {memoize} from 'lodash-es';

import {image} from './utils';

export class Image extends PureComponent {

  componentDidMount() {
    const {file, source} = this.props;

    if (source || !file || !(file instanceof Blob)) return;

    image(this.image, URL.createObjectURL(file));
  }

  element = element =>
    this.image = element;

  render() {
    const {source, ...rest} = this.props;

    return <img ref={this.element} src={source || ''} {...rest} />;
  }
}

export default memoize((date, {file, remove, uploaded}) => (
  <div key={date} className="ui fluid card">
    <div className="image">
      <Image file={file} uploaded={uploaded} />
    </div>

    <div className="content">
      <i className="right floated close icon" onClick={() => remove(file)} />
      <small className="header truncate">{file.name}</small>
    </div>

    <div className="extra content">
      <span className="left floated size">{(file.size / Math.pow(1000, 2)).toFixed(2)} Mb</span>
      <span className="right floated type">{file.type}</span>
    </div>
  </div>
));
