import React from 'react';
import Proptypes from 'prop-types';
import {connect} from 'react-redux';

import {
  branch,
  compose,
  renderComponent,
} from 'recompose';

import {ROUTES} from 'Core/constants';
import {userSignOut} from 'Core/user/redux/actions';

import {
  DropDown,
  DropDownItem,
  DropDownHeader,
  DropDownContent,

  AccountIcon,
  AccountLink,
  AccountInfoItem,
  AccountInfoText,
  AccountContainer,
  AccountInfoContainer,
  AccountFullContainer,
} from './Account.styled';

function Signed({user: {email}, signout}) {
  return (      
    <DropDown>
      <AccountInfoItem>
        <AccountInfoText>{email}</AccountInfoText>
        <AccountIcon className="user large alternate icon"/>
      </AccountInfoItem>
      <DropDownContent>
        <DropDownHeader>
          <AccountInfoText>Account</AccountInfoText>
        </DropDownHeader>
        <AccountLink to="/you/publications/website">
          <DropDownItem>
            <AccountIcon className="users icon" />
            <AccountInfoText>My publications</AccountInfoText>
          </DropDownItem>
        </AccountLink>
        <AccountLink disabled to="/you/favorites">
          <DropDownItem disabled>
            <AccountIcon className="star icon" />
            <AccountInfoText>Favorites</AccountInfoText>
          </DropDownItem>  
        </AccountLink>
        <DropDownHeader>
          <AccountInfoText>Settings</AccountInfoText>
        </DropDownHeader>
        <DropDownItem onClick={signout}>
          <AccountIcon className="sign out alternate icon" />
          <AccountInfoText>Sign out</AccountInfoText>
        </DropDownItem> 
      </DropDownContent>
    </DropDown>
  );
}

Signed.propTypes = {
  user: Proptypes.shape({
    id: Proptypes.string,
    name: Proptypes.string,
    surname: Proptypes.string,
    username: Proptypes.string,
  }).isRequired,

  signout: Proptypes.func.isRequired,
};

Signed.defaultProps = {};

function Signin() {
  return (
    <AccountFullContainer>
      <AccountInfoContainer>
        <AccountIcon key="icon" className="lock large alternate icon"/>
        <AccountLink to={ROUTES.SIGN.IN}>
          <AccountInfoText>Sign in</AccountInfoText>
        </AccountLink>
      </AccountInfoContainer>
    </AccountFullContainer>
  );
}

Signin.propTypes = {};

Signin.defaultProps = {};

const EnhancedUser = compose(
  connect(
    state => ({
      user: state.user,
    }),

    dispatch => ({
      signout: () =>
        dispatch(userSignOut()),
    }),
  ),
  branch(
    ({user}) => Boolean(user.id),
    renderComponent(Signed),
    renderComponent(Signin),
  ),
)();

export default function Header(){
  return (
    <AccountContainer>
      <AccountInfoContainer>
        <AccountInfoContainer>
          <AccountLink disabled to="/you/messages">
            <span>Favorites</span>
          </AccountLink>
        </AccountInfoContainer>
        <AccountInfoContainer>
          <AccountLink target="_blank" to="#">
            <span>Blog</span>
          </AccountLink>
        </AccountInfoContainer>
      </AccountInfoContainer>
      <AccountInfoContainer>
        <EnhancedUser />
      </AccountInfoContainer>  
    </AccountContainer>   
  )
}
