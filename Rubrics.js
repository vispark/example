import React, {Fragment} from 'react';
import {branch, compose, withProps, renderComponent} from 'recompose';

import {Rubrics, Categories} from 'Components/Rubrics';
import Toggle from 'Core/utilities/hocs/toggle';

import {ACTIONS} from '../core/constants';
import {cx} from 'react-emotion';

import {
  Action,
  Actions,
  Container,
} from '../views/styled';

function DefaultRubrics(){
  return (
    <div className="container">
      <Rubrics decent />
    </div>
  );
}

function DefaultCategories({active, toggle}){
  return (
    <Fragment>
      <Actions className="container">
        <Action onClick={toggle}>
          <span>See also</span>
          <i className={cx(
            'large icon arrow alternate circle outline', {
              'up': active,
              'down': !active,
            })} />
        </Action>
      </Actions>
      <Container active={active}>
        <Categories />
      </Container>
    </Fragment>
  );
}

const EnchancedCategories = compose(
  withProps(() => ({active: true})),
  Toggle
)(DefaultCategories);

export default compose(
  branch(
    ({action}) => action === ACTIONS.DEFAULT_RUBRICS,
    renderComponent(DefaultRubrics),
  ),
  branch(
    ({action}) => action === ACTIONS.EXTENDED_CATEGORIES,
    renderComponent(EnchancedCategories),
  )
)(Categories);