import Proptypes from 'prop-types';
import {Container} from 'unstated';
import {
  compose,
  withHandlers,
  setPropTypes,
} from 'recompose';

import request from 'Core/utilities/request-processing';

export default compose(
  setPropTypes({
    container: Proptypes.instanceOf(
      Container
    ).isRequired,
    endpoint: Proptypes.func.isRequired,
  }),

  withHandlers({
    receive({container: {state, patch}, endpoint}) {
      return () => {
        request.observable(endpoint, state.filters)
          .subscribe(response => {
            const {advertisements, meta} = response;
            
            patch({
              cursor: meta,
              loading: false,
              items: advertisements,
            });
          });
      };
    },
  }),
);
