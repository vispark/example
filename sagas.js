import {
  put,
  call,
  take,
  fork,
  actionChannel,
} from 'redux-saga/effects';

import history from 'Core/history';
import {ROUTES} from 'Core/constants';

import {USER} from 'Core/user/redux/actions';
import {application} from 'Core/application/actions';

import {
  DRAFT,
  draftSaveActions,
  draftCleanActions,
  draftRestoreActions,
} from './actions';

import {set, get, clear, inspect} from '../storage';

function* save() {
  const channel = yield actionChannel(DRAFT.SAVE_REQUESTED);

  while (true) {
    try {
      const {payload: {form}} = yield take(channel);
      yield call(set, form);
      yield put(draftSaveActions.succeeded());
    } catch (error) {
      yield put(application.crashed(error));
    }
  }
}

function* clean() {
  const channel = yield actionChannel(DRAFT.CLEAN_REQUESTED);

  while (true) {
    try {
      yield take(channel);
      yield call(clear);
      yield put(draftCleanActions.succeeded());
    } catch (error) {
      yield put(application.crashed(error));
    }
  }
}

function* restore() {
  const channel = yield actionChannel(DRAFT.RESTORE_REQUESTED);

  while (true) {
    try {
      const {payload: {form}} = yield take(channel);
      const restored = yield call(get, form);

      yield put(draftRestoreActions.succeeded(form));
      yield put(draftCleanActions.request());

      // if (history.location.pathname !== ROUTES.ADVERT.PUBLISH.WEBSITE) {
      //   yield call(history.push, ROUTES.ADVERT.PUBLISH.WEBSITE, restored);
      // }

      yield call(history.replace, ROUTES.ADVERT.PUBLISH.WEBSITE, restored);

    } catch (error) {
      yield put(application.crashed(error));
    }
  }
}

function* check() {
  const channel = yield actionChannel([
    USER.AUTHENTICATION_SUCCEEDED,
    USER.AUTHORIZATION_SUCCEEDED,
    USER.REGISTRATION_SUCCEEDED,
  ]);

  while (true) {
    try {
      yield take(channel);
      const [form] = yield call(inspect);

      if (form) {
        yield put(draftRestoreActions.request(form));
      }
    } catch (error) {
      yield put(application.crashed(error));
    }
  }
}

export default [
  fork(save),
  fork(clean),
  fork(restore),
  fork(check),
];
